package org.example.Character;

import org.example.Items.Armor;
import org.example.Items.Weapon;
import org.example.exceptions.InvalidArmorException;
import org.example.exceptions.InvalidWeaponException;

public class Warrior extends Character {
    // Fields -state
    private static int Strength=3;
    private static int Dexterity=2; //Created with the proper default attributes. //
    private static int Intelligence=1;
    private Armor armor;
   private static PrimaryAttributes skill= new PrimaryAttributes(Strength,Dexterity,Intelligence);
    private Weapon weapon;
    //Constructor
    public Warrior() {
    }

    public Warrior(String Name) {
        super(Name, skill);

    }
    //Methods
    public Armor getArmor() {
        return armor;
    }


    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public void setArmor(Armor armor) {
        this.armor = armor;
    }


    //Equipping weapon
    public void equipment (Weapon weapon)throws InvalidWeaponException {
        if(weapon.getWeaponsType() == Weapon.WeaponType.Axes|| weapon.getWeaponsType() == Weapon.WeaponType.Swords || weapon.getWeaponsType() ==Weapon.WeaponType.Hammers) {
            setWeapon(weapon);
        }
        else{
            throw new InvalidWeaponException(getName() + " can't equip " + weapon.getWeaponsType()+ ",wrong type.");
        }
    }
    public void equipment (Armor armor)throws InvalidArmorException {
        if(armor.getArmorType() ==Armor.ArmorType.MAIL|| armor.getArmorType() ==Armor.ArmorType.PLATE ) {
           setArmor(armor);

          this.getBasePrimaryAttributes().setStrength(this.getBasePrimaryAttributes().getStrength()+armor.getAttributes().getStrength());
           this.getBasePrimaryAttributes().setStrength(this.getBasePrimaryAttributes().getDexterity()+armor.getAttributes().getDexterity());
           this.getBasePrimaryAttributes().setStrength(this.getBasePrimaryAttributes().getIntelligence()+armor.getAttributes().getIntelligence());
        }
        else{
            throw new InvalidArmorException(getName() + " can't equip " + armor.getArmorType()+ ",wrong type.");

        }
    }
    public void levelUp () {
     setLevel();
     int b=2;
     this.Strength+=b;
     addStrength(Strength);
    }



    public double characterDPS(){
        // double Y fixes decimals
        double y = 100;
        return (1 + (getBasePrimaryAttributes().getStrength() / y));
    }

    public double totalDPS(Weapon weapon){
        double y = 100;
        return ((weapon.getDps())*(characterDPS()));
    }




}
