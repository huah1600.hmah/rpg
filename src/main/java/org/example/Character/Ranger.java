package org.example.Character;

public class Ranger extends Character  {
    private static int Strength=1;
    private static int Dexterity=5;
    private static int Intelligence=1;

    private static PrimaryAttributes skill= new PrimaryAttributes(Strength,Dexterity,Intelligence);

    public  Ranger() {
    }

    public Ranger(String Name) {
        super(Name, skill);

    }
    public void levelUp () {
        setLevel();
        int b=2;
        this.Dexterity+=b;
        addDexterity(Dexterity);
    }
}
