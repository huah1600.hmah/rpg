package org.example.Character;

public class Mage extends Character {
    private static int Strength=1;
    private static int Dexterity=1;
    private static int Intelligence=5;

    private static PrimaryAttributes skill= new PrimaryAttributes(Strength,Dexterity,Intelligence);

    public Mage() {
    }

    public Mage(String Name) {
        super(Name, skill);

    }
    public void levelUp () {
        setLevel();
        int b=3;
        this.Intelligence+=b;
        addIntelligence(Intelligence);
    }
}
