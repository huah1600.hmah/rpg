package org.example.Character;

public class PrimaryAttributes {

    private int Strength;
    private int Dexterity;
    private int Intelligence;

    public PrimaryAttributes(int Strength, int Dexterity, int Intelligence) {
        this.Strength = Strength;
        this.Dexterity = Dexterity;
        this.Intelligence = Intelligence;
    }

    public int getStrength() {
        return Strength;
    }

    public int getDexterity() {
        return Dexterity;
    }
    public int getIntelligence() {
        return Intelligence;
    }

    public void setStrength(int Strength) {
        this.Strength = Strength;
    }

    public void setDexterity(int Dexterity) {

        this.Dexterity = Dexterity;
    }

    public void setIntelligence(int Intelligence) {

        this.Intelligence = Intelligence;
    }



    @Override
    public String toString() {
        return " " +
                "Strength=" + Strength +
                "  Dexterity=" + Dexterity +
                "  Intelligence=" + Intelligence +
                "";
    }
}
