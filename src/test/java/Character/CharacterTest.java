package Character;
import org.example.Character.Mage;
import org.example.Character.Ranger;
import org.example.Character.Rouge;
import org.example.Character.Warrior;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CharacterTest {
  Warrior testWarrior;
  Ranger testRanger;
  Mage testMage;
  Rouge testRouge;
  @BeforeEach
  public void  createPlayer(){

    testWarrior= new Warrior("Warrior");
    testRanger= new Ranger(" Ranger");
    testMage= new Mage("Mage");
    testRouge= new Rouge("Rouge");


  }

  //1) A character is level 1 when created.
  @Test
  public void CheckLevelWhenStarting_ValidValue_ExpectedBehaviour(){


    int testLevel= testWarrior.getLevel();
    int expected =1;


    assertEquals(expected,testLevel);

  }

  //2) When a character gains a level, it should be level 2.
  @Test
  public void  CheckLevelWhenLevelUp_ValidValue_ExpectedBehaviour(){


    testWarrior.setLevel();
    int testLevelUp= testWarrior.getLevel();
    int expected =2;

    assertEquals(expected,testLevelUp);

  }
  //3) Each character class is created with the proper default attributes.
  @Test
  public void CheckWarriorAttributes_ValidValue_ExpectedBehaviour(){


    int strength= 3;
    int dexterity =2;
    int intelligence =1;
    String expected =" basePrimaryAttributes=" + " Strength=" + strength+ "  Dexterity=" + dexterity +"  Intelligence="+ intelligence;


    assertEquals(expected,testWarrior.getPlayerAttributes());

  }
  @Test
  public void CheckMageAttributes_ValidValue_ExpectedBehaviour(){


    int strength= 1;
    int dexterity =1;
    int intelligence =5;
    String expected =" basePrimaryAttributes=" + " Strength=" + strength+ "  Dexterity=" + dexterity +"  Intelligence="+ intelligence;


    assertEquals(expected,testMage.getPlayerAttributes());

  }
  @Test
  public void CheckRangerAttributes_ValidValue_ExpectedBehaviour(){


    int strength= 1;
    int dexterity =5;
    int intelligence =1;
    String expected =" basePrimaryAttributes=" + " Strength=" + strength+ "  Dexterity=" + dexterity +"  Intelligence="+ intelligence;


    assertEquals(expected,testRanger.getPlayerAttributes());

  }
  @Test
  public void CheckRougeAttributes_ValidValue_ExpectedBehaviour(){


    int strength= 1;
    int dexterity =4;
    int intelligence =1;
    String expected =" basePrimaryAttributes=" + " Strength=" + strength+ "  Dexterity=" + dexterity +"  Intelligence="+ intelligence;


    assertEquals(expected,testRouge.getPlayerAttributes());

  }
  //4) Each character class has their attributes increased when leveling up.
  @Test
  public void CheckWarriorIncreasedAttributes_ValidValue_ExpectedBehaviour(){


    testWarrior.levelUp();
    int strength= 5;
    int dexterity =2;
    int intelligence =1;
    String expected =" basePrimaryAttributes=" + " Strength=" + strength+ "  Dexterity=" + dexterity +"  Intelligence="+ intelligence;


    assertEquals(expected,testWarrior.getPlayerAttributes());

  }

  @Test
  public void CheckRougeIncreasedAttributes_ValidValue_ExpectedBehaviour(){


    testRouge.levelUp();
    int strength= 2;
    int dexterity =6;
    int intelligence =1;
    String expected =" basePrimaryAttributes=" + " Strength=" + strength+ "  Dexterity=" + dexterity +"  Intelligence="+ intelligence;


    assertEquals(expected,testRouge.getPlayerAttributes());

  }


  @Test
  public void CheckRangerIncreasedAttributes_ValidValue_ExpectedBehaviour(){


    testRanger.levelUp();
    int strength= 1;
    int dexterity =7;
    int intelligence =1;
    String expected =" basePrimaryAttributes=" + " Strength=" + strength+ "  Dexterity=" + dexterity +"  Intelligence="+ intelligence;


    assertEquals(expected,testRanger.getPlayerAttributes());

  }


  @Test
  public void CheckMageIncreasedAttributes_ValidValue_ExpectedBehaviour(){


    testMage.levelUp();
    int strength= 1;
    int dexterity =1;
    int intelligence =8;
    String expected =" basePrimaryAttributes=" + " Strength=" + strength+ "  Dexterity=" + dexterity +"  Intelligence="+ intelligence;


    assertEquals(expected,testMage.getPlayerAttributes());

  }
}