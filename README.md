### This program runs on a console. This application gives you the following options:

1.making characters

2.They can level up

3.Items can be created.

4.equip objects

### Minor bugs
I discovered in the CharacterTest file that two functions must be performed separately and cannot, for some reason, be executed simultaneously with  the other test functions.

### Programming language
Java was utilized as a programming language.

### Unit Testing

Unit testing in JUnit 5 is built into this app, and there are test methods in this project. The goal of the tests is to see how well the console application works.


Licence
Copyright 2022, Noroff Accelerate AS

