package org.example.Items;

import org.example.Character.PrimaryAttributes;

public class Armor extends Items {
    // Fields -state
    private PrimaryAttributes attributes;
    private ArmorType armor;
    public enum ArmorType {
        CLOTH,LEATHER ,MAIL ,PLATE

    }



    public Armor() {
    }
    public Armor(String name , int level , Slot slotType, ArmorType armor, PrimaryAttributes attributes){
        super(name, level, slotType);
        this.armor=armor;
        this.attributes=attributes;

    }
    //Methods
    public PrimaryAttributes getAttributes() {
        return attributes;
    }

    public void setAttributes(PrimaryAttributes attributes) {
        this.attributes = attributes;
    }

    public ArmorType getArmor() {
        return armor;
    }

    public void setArmor(ArmorType armor) {
        this.armor = armor;
    }


    public ArmorType getArmorType() {
        return armor;
    }

    @Override
    public String toString() {
        return   super.toString()+
                " Armor=" + armor+
                " attributes=" + attributes;
    }
}
