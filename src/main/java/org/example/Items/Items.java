package org.example.Items;

public abstract class  Items  {

    // Fields -state
    protected String Name;
    protected int requirementLevel;
    protected Slot Slot;

    private Weapon.WeaponType WeaponsType;
    //Constructer
    public Items() {
    }

    public Items(String Name, int requirementLevel, Slot Slot) {
        this.Name = Name;
        this.requirementLevel = requirementLevel;
        this.Slot=Slot;

    }

    //Methods
    public void setName(String name) {
        Name = name;
    }

    public void setLevel(int level) {
        requirementLevel = level;
    }
    public void setSlot(Slot slot) {
        Slot = slot;
    }

    public String getName() {
        return Name;
    }

    public int getRequirementLevel() {
        return requirementLevel;
    }

    public Slot getSlot() {
        return Slot;
    }

    @Override
    public String toString() {
        return
                "Name=" + Name +
                " Level=" + requirementLevel +
                " Slot=" + Slot;

    }
}

