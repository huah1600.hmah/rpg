package org.example.Character;

import org.example.Items.Armor;
import org.example.Items.Weapon;
import org.example.exceptions.InvalidArmorException;
import org.example.exceptions.InvalidWeaponException;

public abstract class Character {

    // Fields -state
    private String Name;
    private int Level;
    private PrimaryAttributes basePrimaryAttributes;
    protected double characterDps;

   //Constructor
     private double charDps;
    public Character() {
    }
    public Character(String Name,PrimaryAttributes basePrimaryAttributes) {
        this.Name = Name;
        this.Level = 1;
        this.basePrimaryAttributes= basePrimaryAttributes;

    }



    //Methods

    public String getName() {
        return Name;
    }



    public void setName(String name) {
        this.Name = Name;
    }

    public void setLevel() {
        this.Level++;
    }
    public void addStrength(int s){
        basePrimaryAttributes.setStrength(s);
    }
    public void addDexterity(int d){
        basePrimaryAttributes.setDexterity(d);
    }
    public void addIntelligence(int i){
        basePrimaryAttributes.setIntelligence(i);
    }
    public void setBasePrimaryAttributes(PrimaryAttributes basePrimaryAttributes) {
        this.basePrimaryAttributes = basePrimaryAttributes;
    }
    public void setEquip(Weapon weapon)throws InvalidWeaponException {
  //check level
        if(weapon.getRequirementLevel() <= this.Level){


        }
        else{
            throw new InvalidWeaponException("The level of the weapon is to high for " +this.getName());
        }

    }
    public void setEquip(Armor armor)throws InvalidArmorException {
        //check level
        if(armor.getRequirementLevel() <= this.Level){


        }
        else{
            throw new InvalidArmorException("The level of the armor is to high for " +this.getName());
        }

    }
    public PrimaryAttributes getBasePrimaryAttributes() {
        return  basePrimaryAttributes;
    }

    public int getLevel() {
        return Level;
    }

    @Override
    public String toString() {
        return "Character{" +
                "Name='" + Name + '\'' +
                ", Level=" + Level +
                ", basePrimaryAttributes=" + basePrimaryAttributes +
                '}';
    }

    public String getPlayerAttributes() {
        return " basePrimaryAttributes=" + basePrimaryAttributes;
    }
}


