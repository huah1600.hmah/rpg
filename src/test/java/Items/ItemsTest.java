package Items;

import org.example.Character.PrimaryAttributes;
import org.example.Character.Warrior;
import org.example.Items.Armor;
import org.example.Items.Slot;
import org.example.Items.Weapon;
import org.example.exceptions.InvalidArmorException;
import org.example.exceptions.InvalidWeaponException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ItemsTest {
    Warrior testWarrior;

    @BeforeEach
    public void createPlayer() {

        testWarrior = new Warrior("Husow");


    }

    //1).If a character tries to equip a high level weapon, InvalidWeaponException should be thrown.
    @Test
    public void CheckWrongLevelWeapon_ValidValue_ExpectedThrowInvalidException() {


        Weapon testWeapon = new Weapon("Common Axe", 2, Slot.WEAPON, Weapon.WeaponType.Swords, 7, 1.1);
        String expected = "The level of the weapon is to high for " + testWarrior.getName();
        InvalidWeaponException exception = assertThrows(InvalidWeaponException.class, () -> testWarrior.setEquip(testWeapon));
        String actual = exception.getMessage();

        assertEquals(expected,actual);

    }

    //2) If a character tries to equip a high level armor piece, InvalidArmorException should be thrown
    @Test
    public void CheckWrongLevelArmor_ValidValue_ExpectedThrowInvalidException() {


        Armor testPlateBody = new Armor("Common Plate Body Armor", 2, Slot.BODY, Armor.ArmorType.PLATE, new PrimaryAttributes(1, 0, 0));
        String expected = "The level of the armor is to high for " + testWarrior.getName();
        InvalidArmorException exception = assertThrows(InvalidArmorException.class, () -> testWarrior.setEquip(testPlateBody));
        String actual = exception.getMessage();

        assertEquals(expected, actual);

    }

    //3) If a character tries to equip the wrong weapon type, InvalidWeaponException should be thrown.
    @Test
    public void CheckWrongWeaponType_ValidValue_ExpectedThrowInvalidException() {


        Weapon testWeapon = new Weapon("Common Axe", 2, Slot.WEAPON, Weapon.WeaponType.Bows, 7, 1.1);
        String expected = testWarrior.getName() + " can't equip " + testWeapon.getWeaponsType() + ",wrong type.";
        InvalidWeaponException exception = assertThrows(InvalidWeaponException.class, () -> testWarrior.equipment(testWeapon));
        String result = exception.getMessage();

        assertEquals(expected, result);

    }


    //4) If a character tries to equip the wrong armor type, InvalidArmorException should be thrown.
    @Test
    public void CheckWrongArmorType_ValidValue_ExpectedThrowInvalidException() {


        Armor testPlateBody = new Armor("Common Plate Body Armor", 1, Slot.BODY, Armor.ArmorType.CLOTH, new PrimaryAttributes(1, 0, 0));
        String expected = testWarrior.getName() + " can't equip " + testPlateBody.getArmorType() + ",wrong type.";
        InvalidArmorException exception = assertThrows(InvalidArmorException.class, () -> testWarrior.equipment(testPlateBody));
        String actual = exception.getMessage();

        assertEquals(expected,actual);

    }

    //5) If a character equips a valid weapon, a Boolean true should be returned.
    @Test
    public void CheckCharacterEquipsWeaponType_ValidValue_ExpectedTrue() throws InvalidWeaponException {

        Weapon testWeapon = new Weapon("Common Axe", 2, Slot.WEAPON, Weapon.WeaponType.Axes, 7, 1.1);

        testWarrior.equipment(testWeapon);
        assertTrue(testWarrior.getWeapon() != null);

    }

    //6) If a Character equips a valid armor piece, a Boolean true should be returned.
    @Test
    public void CheckCharacterEquipsArmorType_ExpectedBehaviour() throws InvalidArmorException {

        Armor testPlateBody = new Armor("Common Plate Body Armor", 2, Slot.BODY, Armor.ArmorType.PLATE, new PrimaryAttributes(1, 0, 0));

        testWarrior.equipment(testPlateBody);
        assertTrue(testWarrior.getArmor() != null);

    }

    //7) Calculate DPS if no weapon is equipped.
    @Test
    void CalculateDPS_ValidValue_ExpectedToExpectedBehaviour() {
        double y = 100;
        double actual = testWarrior.characterDPS();
        double expected = 1 * (1 + (5 / y));
        assertEquals(Math.round(expected), Math.round(actual));
    }
//8) Calculate DPS with valid weapon equipped.
    @Test
    void CalculateDPSWithWeapon_ValidValue_ExpectedBehaviour() {
        double y = 100;
        Weapon testWeapon = new Weapon("Common Axe", 1, Slot.WEAPON, Weapon.WeaponType.Axes, 7, 1.1);

        testWarrior.setWeapon(testWeapon);
        double actual = testWarrior.totalDPS(testWeapon);
        double expected = (7 * 1.1) * (1 + (5 / y));
        assertEquals(Math.round(expected), Math.round(actual));
    }
//9) Calculate DPS with valid weapon and armor equipped.
    @Test
    void CalculateTotalDPS_ValidValue_ExpectedBehaviour() throws InvalidArmorException {
        double y = 100;
        Weapon testWeapon = new Weapon("Common Axe", 1, Slot.WEAPON, Weapon.WeaponType.Axes, 7, 1.1);
        Armor testPlateBody = new Armor("Common Plate Body Armor", 1, Slot.BODY, Armor.ArmorType.PLATE, new PrimaryAttributes(1, 0, 0));
        testWarrior.setWeapon(testWeapon);
        testWarrior.equipment(testPlateBody);
        double actual = testWarrior.totalDPS(testWeapon);
        double expected = (7 * 1.1) * (1 + ((5 + 1) / y));

        assertEquals(Math.round(expected), Math.round(actual));
    }

}
