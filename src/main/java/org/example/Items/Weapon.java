package org.example.Items;

public class Weapon extends Items {
    private String name;
    private int level;

    private Slot slot;
    private int damage;
    private double attackSpeed;
    private WeaponType WeaponsType;
    private double dps;
    public enum WeaponType{

        Axes,Bows,Daggers,Hammers ,Staffs ,Swords,Wands;
    }


    public Weapon() {
    }

    public Weapon(String name, int level, Slot slot, WeaponType WeaponsType, int damage, double attackSpeed) {
        super(name, level, slot);

        this.damage = damage;
        this.attackSpeed = attackSpeed;
        this.WeaponsType=WeaponsType;
        this.dps=damage*attackSpeed;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public void setAttackSpeed(float attackSpeed) {
        this.attackSpeed = attackSpeed;
    }

    public int getDamage() {
        return damage;
    }

    public double getAttackSpeed() {
        return attackSpeed;
    }

    public double getDps() {

        return damage*attackSpeed;
    }

    public void setDps(double dps) {
        this.dps = dps;
    }

    public WeaponType getWeaponsType() {
        return WeaponsType;
    }

    public String toString() {
        return super.toString()+
                " WeaponsType= "+ WeaponsType +
                " Damage=" + damage  +
                " AttackSpeed=" + attackSpeed;
    }

}
