package org.example.Character;

public class Rouge extends Character {


    private static int Strength=1;
    private static int Dexterity=4;
    private static int Intelligence=1;

    private static PrimaryAttributes skill= new PrimaryAttributes(Strength,Dexterity,Intelligence);

    public Rouge() {
    }

    public Rouge(String Name) {
        super(Name, skill);

    }
    public void levelUp () {
        setLevel();
        int b=1;
        int a=2;
        this.Strength+=b;
        this.Dexterity+=a;
        addStrength(Strength);
        addDexterity(Dexterity);
    }

}
